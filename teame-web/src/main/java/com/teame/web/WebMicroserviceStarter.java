package com.teame.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebMicroserviceStarter {
    public static void main(String[] args){
        SpringApplication.run(WebMicroserviceStarter.class, args);
    }
}
