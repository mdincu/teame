"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var MctestComponent = /** @class */ (function () {
    function MctestComponent(httpClient) {
        this.httpClient = httpClient;
        this.testTemplates = [];
    }
    MctestComponent.prototype.ngOnInit = function () {
        var _this = this;
        var headers = new http_1.HttpHeaders({ 'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': 'Origin', 'enctype': 'multipart/form-data' });
        this.httpClient
            .get('http://localhost:8080/login/mctests/mctestcards', { headers: headers })
            .subscribe(function (response) {
            _this.testTemplates = response;
            console.log('POST call successful value returned in body', response);
        }, function (error) {
            console.log('POST call in error modified', error);
        }, function () {
            console.log('The POST observable is now completed.');
        });
        /*let testTemplate1: TestcardComponent  = new TestcardComponent();
        testTemplate1.id = 'id';
        testTemplate1.title = 'title1';
        testTemplate1.description = 'description';
    
        let testTemplate2: TestcardComponent = new TestcardComponent();
        testTemplate2.id = 'id';
        testTemplate2.title = 'title2';
        testTemplate2.description = 'description';
    
        this.testTemplates.push(testTemplate1);
        this.testTemplates.push(testTemplate2);
        */
    };
    MctestComponent = __decorate([
        core_1.Component({
            selector: 'app-mctest',
            templateUrl: './mctest.component.html',
            styleUrls: ['./mctest.component.css']
        }),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], MctestComponent);
    return MctestComponent;
}());
exports.MctestComponent = MctestComponent;
//# sourceMappingURL=mctest.component.js.map