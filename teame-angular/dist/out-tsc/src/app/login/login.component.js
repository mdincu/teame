"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var LoginComponent = /** @class */ (function () {
    function LoginComponent(httpClient) {
        this.httpClient = httpClient;
        this.loginBtn = 'Login';
        this.signupBtn = 'Sign Up';
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.performLogin = function () {
        var headers = new http_1.HttpHeaders({ 'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': 'Origin', 'enctype': 'multipart/form-data' });
        var formData = new FormData();
        formData.append('email', this.email);
        formData.append('password', this.password);
        this.httpClient
            .post('http://localhost:8080/login', formData, { headers: headers })
            .subscribe(function (response) {
            console.log('POST call successful value returned in body', response);
        }, function (error) {
            console.log('POST call in error modified', error);
        }, function () {
            console.log('The POST observable is now completed.');
        });
    };
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'app-login',
            templateUrl: './login.component.html',
            styleUrls: ['./login.component.css']
        }),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=login.component.js.map