"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var auth_interceptor_1 = require("./auth.interceptor");
var http_1 = require("@angular/common/http");
var http_2 = require("@angular/common/http");
var app_component_1 = require("./app.component");
var app_routing_module_1 = require("./app-routing.module");
var home_component_1 = require("./home/home.component");
var navbar_component_1 = require("./home/candidate/navbar/navbar.component");
var candidate_component_1 = require("./home/candidate/candidate.component");
var recruiter_component_1 = require("./home/recruiter/recruiter.component");
var mctest_component_1 = require("./home/candidate/mctest/mctest.component");
var testtemplate_component_1 = require("./home/candidate/mctest/testtemplate/testtemplate.component");
var testcard_component_1 = require("./home/candidate/mctest/testcard/testcard.component");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                app_routing_module_1.routingComponents,
                home_component_1.HomeComponent,
                navbar_component_1.NavbarComponent,
                candidate_component_1.CandidateComponent,
                recruiter_component_1.RecruiterComponent,
                mctest_component_1.MctestComponent,
                testtemplate_component_1.TesttemplateComponent,
                testcard_component_1.TestcardComponent
            ],
            imports: [
                platform_browser_1.BrowserModule,
                forms_1.FormsModule,
                app_routing_module_1.AppRoutingModule,
                http_2.HttpClientModule
            ],
            providers: [
                {
                    provide: http_1.HTTP_INTERCEPTORS,
                    useClass: auth_interceptor_1.AuthInterceptor,
                    multi: true
                }
            ],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map