import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  signupBtn: string;
  email: string;
  password: string;

  constructor(private httpClient: HttpClient) {
    this.signupBtn = 'Signup';
  }

  ngOnInit() {
  }

  performSignup(): void {
    const headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Origin'
    });

    this.httpClient
      .post('http://localhost:8080/signup', {email: this.email, password: this.password}, {headers: headers})
      .subscribe(
        response => {
          console.log('POST call successful value returned in body', response);
        },
        error => {
          console.log('POST call in error modified', error);
        },
        () => {
          console.log('The POST observable is now completed.');
        });
  }

}
