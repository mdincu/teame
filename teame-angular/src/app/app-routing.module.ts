import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login/login.component';
import {SignupComponent} from './signup/signup.component';
import {HomeComponent} from './login/candidate/home/home.component';
import {MctestcardsComponent} from './login/candidate/mctest/mctestcards/mctestcards.component';
import {MctestComponent} from './login/candidate/mctest/mctest.component';
import {MctestnewquestionComponent} from './login/candidate/mctest/mctestnewquestion/mctestnewquestion.component';
import {TopicsComponent} from './login/candidate/home/topics/topics.component';
import {CandidateComponent} from './login/candidate/candidate.component';
import {TopicComponent} from './login/candidate/home/topics/topic/topic.component';
import {SubjectsComponent} from './login/candidate/home/topics/topic/subjects/subjects.component';
import {SubjectComponent} from './login/candidate/home/topics/topic/subjects/subject/subject.component';
import {CategoriesComponent} from './login/candidate/home/topics/topic/subjects/subject/categories/categories.component';
import {CategoryComponent} from './login/candidate/home/topics/topic/subjects/subject/categories/category/category.component';

const routes: Routes = [
  {path : 'signup', component: SignupComponent},
  {path : 'login', component: LoginComponent},
  {path : 'mctestnewquestions', component: MctestnewquestionComponent},
  {path : 'candidate', component: CandidateComponent, children: [
      {path : 'home', component: HomeComponent},
      {path : 'topics', component: TopicsComponent},
      {path : 'topic/:identifier', component: TopicComponent},
      {path : 'subjects', component: SubjectsComponent},
      {path : 'subject/:identifier', component: SubjectComponent},
      {path : 'categories', component: CategoriesComponent},
      {path : 'category/:identifier', component: CategoryComponent},
      {path : 'mctests', component: MctestcardsComponent},
      {path : 'mctest/:identifier', component: MctestComponent}
   ]}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { useHash: true })
  ],
  exports : [
    RouterModule
  ]
})
export class AppRoutingModule { }
export const routingComponents = [
  LoginComponent,
  SignupComponent,
  HomeComponent,
  MctestComponent,
  MctestcardsComponent,
  MctestnewquestionComponent,
  TopicsComponent,
  TopicComponent,
  SubjectsComponent,
  SubjectComponent,
  CandidateComponent
];
