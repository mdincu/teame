import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';
import { AuthInterceptor } from './auth.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { AppRoutingModule, routingComponents } from './app-routing.module';
import { HomeComponent } from './login/candidate/home/home.component';
import { NavbarComponent } from './login/candidate/navbar/navbar.component';
import { CandidateComponent } from './login/candidate/candidate.component';
import { RecruiterComponent } from './login/recruiter/recruiter.component';
import { MctestcardsComponent } from './login/candidate/mctest/mctestcards/mctestcards.component';
import { MctestcardComponent } from './login/candidate/mctest/mctestcards/mctestcard/mctestcard.component';
import { MctestquestionComponent } from './login/candidate/mctest/mctestcards/mctestcard/mctestquestion/mctestquestion.component';
import { MctestchoiceComponent } from './login/candidate/mctest/mctestcards/mctestcard/mctestquestion/mctestchoice/mctestchoice.component';
import { MctestComponent } from './login/candidate/mctest/mctest.component';
import { MctestnewquestionComponent } from './login/candidate/mctest/mctestnewquestion/mctestnewquestion.component';
import { TopicsComponent } from './login/candidate/home/topics/topics.component';
import { TopicComponent } from './login/candidate/home/topics/topic/topic.component';
import { SubjectsComponent } from './login/candidate/home/topics/topic/subjects/subjects.component';
import { SubjectComponent } from './login/candidate/home/topics/topic/subjects/subject/subject.component';
import { CategoriesComponent } from './login/candidate/home/topics/topic/subjects/subject/categories/categories.component';
import { CategoryComponent } from './login/candidate/home/topics/topic/subjects/subject/categories/category/category.component';

@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    HomeComponent,
    NavbarComponent,
    CandidateComponent,
    RecruiterComponent,
    MctestcardsComponent,
    MctestcardComponent,
    MctestquestionComponent,
    MctestchoiceComponent,
    MctestComponent,
    MctestnewquestionComponent,
    TopicsComponent,
    TopicComponent,
    SubjectsComponent,
    SubjectComponent,
    CategoriesComponent,
    CategoryComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
