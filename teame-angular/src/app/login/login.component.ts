import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {
  CanActivate, Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginBtn: string;
  email: string;
  password: string;
  signupBtn: string;

  constructor(private httpClient: HttpClient, private router: Router) {
    this.loginBtn = 'Login';
    this.signupBtn = 'Sign Up';
  }

  ngOnInit() {
  }

  performLogin(): void {
    const headers = new HttpHeaders({'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Origin', 'enctype': 'multipart/form-data'});
    const formData = new FormData();
    formData.append('email', this.email);
    formData.append('password', this.password);

    this.httpClient
      .post('http://localhost:8080/login', formData, {headers: headers})
      .subscribe(
            response => {
              this.router.navigate(['/candidate']);
              console.log('POST call successful value returned in body', response);
            },
            error => {
              console.log('POST call in error modified', error);
            },
            () => {
              console.log('The POST observable is now completed.');
            });
  }

}
