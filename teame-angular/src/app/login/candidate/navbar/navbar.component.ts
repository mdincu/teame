import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  public navigateToTopics(): void {
    this.navigate('/candidate/topics');
  }

  private navigate(route: string): void {
    this.router.navigate([route], {skipLocationChange: true});
  }
}
