import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {McTestQuestionModel} from '../mctestcards/mctestcard/mctestquestion/mctestquestion.model';
import {McTestChoiceModel} from '../mctestcards/mctestcard/mctestquestion/mctestchoice/mctestchoice.model';

@Component({
  selector: 'app-mctestnewquestion',
  templateUrl: './mctestnewquestion.component.html',
  styleUrls: ['./mctestnewquestion.component.css']
})
export class MctestnewquestionComponent implements OnInit {

  questions: McTestQuestionModel [];

  constructor(private httpClient: HttpClient) {
  }

  ngOnInit() {
    this.questions = [];
    this.addNewQuestion();
  }

  public addNewQuestion(): void {
    const mcTestQuestionExternalIn: McTestQuestionModel = new McTestQuestionModel();
    mcTestQuestionExternalIn.choices = [new McTestChoiceModel()];
    this.questions.push(mcTestQuestionExternalIn);
  }

  public addNewChoice(question: McTestQuestionModel): void {
    question.choices.push(new McTestChoiceModel());
  }

  public submit(): void {
    const headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Origin', 'enctype': 'multipart/form-data'
    });

    this.httpClient
      .post('http://localhost:8080/mctest/addnewtest', this.questions, {headers: headers})
      .subscribe(
        response => {
          console.log('POST call successful value returned in body', response);
        },
        error => {
          console.log('POST call in error modified', error);
        },
        () => {
          console.log('The POST observable is now completed.');
        });
  }
}
