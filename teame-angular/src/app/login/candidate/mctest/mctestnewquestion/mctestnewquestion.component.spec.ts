import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MctestnewquestionComponent } from './mctestnewquestion.component';

describe('MctestnewquestionComponent', () => {
  let component: MctestnewquestionComponent;
  let fixture: ComponentFixture<MctestnewquestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MctestnewquestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MctestnewquestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
