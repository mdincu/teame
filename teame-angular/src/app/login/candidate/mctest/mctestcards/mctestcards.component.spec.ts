import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MctestcardsComponent } from './mctestcards.component';

describe('MctestcardsComponent', () => {
  let component: MctestcardsComponent;
  let fixture: ComponentFixture<MctestcardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MctestcardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MctestcardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
