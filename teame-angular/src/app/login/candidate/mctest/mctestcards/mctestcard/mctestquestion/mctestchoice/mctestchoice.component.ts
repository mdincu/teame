import {Component, Input, OnInit} from '@angular/core';
import {McTestChoiceModel} from './mctestchoice.model';

@Component({
  selector: 'app-mctestchoice',
  templateUrl: './mctestchoice.component.html',
  styleUrls: ['./mctestchoice.component.css']
})
export class MctestchoiceComponent implements OnInit {

  @Input() index: number;
  @Input() mcTestChoiceExternalIn: McTestChoiceModel;
  @Input() checked: boolean;

  constructor() {
  }

  ngOnInit() {
  }

  generateCharIndex(): string {
    return String.fromCharCode(97 + this.index);
  }
}
