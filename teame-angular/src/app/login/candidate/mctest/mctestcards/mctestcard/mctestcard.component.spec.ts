import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MctestcardComponent } from './mctestcard.component';

describe('MctestcardComponent', () => {
  let component: MctestcardComponent;
  let fixture: ComponentFixture<MctestcardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MctestcardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MctestcardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
