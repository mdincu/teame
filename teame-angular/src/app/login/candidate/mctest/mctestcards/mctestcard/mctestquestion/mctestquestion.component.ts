import {AfterViewInit, Component, Input, OnInit, Query, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {McTestQuestionModel} from './mctestquestion.model';
import {MctestchoiceComponent} from './mctestchoice/mctestchoice.component';

@Component({
  selector: 'app-mctestquestion',
  templateUrl: './mctestquestion.component.html',
  styleUrls: ['./mctestquestion.component.css']
})
export class MctestquestionComponent implements OnInit {

  @Input() index: number;
  @Input() mcTestQuestionExternalIn: McTestQuestionModel;
  @ViewChildren('mctestchoicecomponents') mctestchoiceComponents: QueryList<MctestchoiceComponent>;

  constructor() {
  }

  ngOnInit() {
  }

  selectUserChoice(index): void {
    const mctestchoiceComponent: MctestchoiceComponent = this.mctestchoiceComponents.find(
      (component: MctestchoiceComponent) => component.index === index);
    if (mctestchoiceComponent.checked) {
      const i: number = this.mcTestQuestionExternalIn.userChoices.indexOf(mctestchoiceComponent.mcTestChoiceExternalIn, 0);
      if (index > -1) {
        this.mcTestQuestionExternalIn.userChoices.splice(i, 1);
        mctestchoiceComponent.checked = ! mctestchoiceComponent.checked;
      }
    } else {
      this.mcTestQuestionExternalIn.userChoices.push(mctestchoiceComponent.mcTestChoiceExternalIn);
      mctestchoiceComponent.checked = ! mctestchoiceComponent.checked;
    }
  }
}
