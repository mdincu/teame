import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MctestchoiceComponent } from './mctestchoice.component';

describe('MctestchoiceComponent', () => {
  let component: MctestchoiceComponent;
  let fixture: ComponentFixture<MctestchoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MctestchoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MctestchoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
