import { McTestChoiceModel } from './mctestchoice/mctestchoice.model';

export class McTestQuestionModel {

  private _identifier: string;
  private _description: string;
  private _choices: McTestChoiceModel[];
  private _userChoices: McTestChoiceModel[];

  constructor() {
  }

  get identifier(): string {
    return this._identifier;
  }

  set identifier(value: string) {
    this._identifier = value;
  }

  get description(): string {
    return this._description;
  }

  set description(value: string) {
    this._description = value;
  }

  get choices(): McTestChoiceModel[] {
    return this._choices;
  }

  set choices(value: McTestChoiceModel[]) {
    this._choices = value;
  }

  get userChoices(): McTestChoiceModel[] {
    return this._userChoices;
  }

  set userChoices(value: McTestChoiceModel[]) {
    this._userChoices = value;
  }
}
