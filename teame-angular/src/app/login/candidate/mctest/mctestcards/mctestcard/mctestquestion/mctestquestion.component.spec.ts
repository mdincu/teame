import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MctestquestionComponent } from './mctestquestion.component';

describe('MctestquestionComponent', () => {
  let component: MctestquestionComponent;
  let fixture: ComponentFixture<MctestquestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MctestquestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MctestquestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
