import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-testcard',
  templateUrl: './mctestcard.component.html',
  styleUrls: ['./mctestcard.component.css']
})
export class MctestcardComponent implements OnInit {

  @Input() identifier: string;
  @Input() title: string;
  @Input() description: string;
  @Input() imageUrl: string;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  public takeTest(): void {
    this.router.navigate(['/mctest', this.identifier], {skipLocationChange: true});
  }
}
