import { Component, OnInit } from '@angular/core';
import {MctestcardComponent} from './mctestcard/mctestcard.component';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-mctest',
  templateUrl: './mctestcards.component.html',
  styleUrls: ['./mctestcards.component.css']
})
export class MctestcardsComponent implements OnInit {

  testTemplates: MctestcardComponent[] = [];

  constructor(private httpClient: HttpClient) {
  }

  ngOnInit() {
    this.getUserMcCards();
  }

  private getUserMcCards(): void {

    const headers = new HttpHeaders({'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Origin', 'enctype': 'multipart/form-data'});

    this.httpClient
      .get('http://localhost:8080/mctest/mctestsubjects', {headers: headers})
      .subscribe(
        (response: MctestcardComponent[]) => {
          this.testTemplates = response;
        },
        error => {
          console.log('POST call in error modified', error);
        },
        () => {
          console.log('The POST observable is now completed.');
        });

  }


}
