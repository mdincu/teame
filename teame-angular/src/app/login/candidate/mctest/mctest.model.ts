import {McTestQuestionModel} from './mctestcards/mctestcard/mctestquestion/mctestquestion.model';

export class MctestModel {

  private _identifier: string;
  private _name: string;
  private _description: string;
  private _questions: McTestQuestionModel [];

  constructor() {}

  get identifier(): string {
    return this._identifier;
  }

  set identifier(value: string) {
    this._identifier = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get description(): string {
    return this._description;
  }

  set description(value: string) {
    this._description = value;
  }

  get questions(): McTestQuestionModel[] {
    return this._questions;
  }

  set questions(value: McTestQuestionModel[]) {
    this._questions = value;
  }
}
