import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MctestComponent } from './mctest.component';

describe('MctestComponent', () => {
  let component: MctestComponent;
  let fixture: ComponentFixture<MctestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MctestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MctestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
