import {Component, OnInit} from '@angular/core';
import {MctestModel} from './mctest.model';
import {ActivatedRoute} from '@angular/router';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

@Component({
  selector: 'app-mctest',
  templateUrl: './mctest.component.html',
  styleUrls: ['./mctest.component.css']
})
export class MctestComponent implements OnInit {

  private identifier: string;
  mcTestExternalIn: MctestModel;

  constructor(private activatedRoute: ActivatedRoute, private httpClient: HttpClient) {
    this.activatedRoute.params.subscribe(params => {
      this.identifier = params['identifier'];
    });
  }

  ngOnInit() {
    const headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Origin', 'enctype': 'multipart/form-data'
    });

    const httpParams: HttpParams = new HttpParams()
      .append('identifier', this.identifier);

    this.httpClient
      .post('http://localhost:8080/mctest/subject', httpParams, {headers: headers})
      .subscribe(
        (mcTestExternalIn: MctestModel) => {
          this.mcTestExternalIn = mcTestExternalIn;
        },
        error => {
          console.log('POST call in error modified', error);
        },
        () => {
          console.log('The POST observable is now completed.');
        });
  }

  submitTest(): void {
    const headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Origin', 'enctype': 'multipart/form-data'
    });

    this.httpClient
      .post('http://localhost:8080/mctest/submit', this.mcTestExternalIn, {headers: headers})
      .subscribe(
        response => {
          console.log(response);
        },
        error => {
          console.log('POST call in error modified', error);
        },
        () => {
          console.log('The POST observable is now completed.');
        });
  }
}
