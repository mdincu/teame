import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TopicsService {

  constructor(private httpClient: HttpClient) {
  }

  getTopics(): Observable<Object> {
    const headers = new HttpHeaders({'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Origin', 'enctype': 'multipart/form-data'});

    const topics = this.httpClient
      .get('http://localhost:8080/topic', {headers: headers});

    return topics;
  }
}
