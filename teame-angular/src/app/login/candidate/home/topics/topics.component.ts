import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {TopicsService} from './topics.service';
import {TopicModelIn} from './topic/topic.model';

@Component({
  selector: 'app-topics',
  templateUrl: './topics.component.html',
  styleUrls: ['./topics.component.css']
})
export class TopicsComponent implements OnInit {

  topics: TopicModelIn [];

  constructor(private httpClient: HttpClient, private topicsService: TopicsService) {
  }

  ngOnInit() {
    this.getUserTopics();
  }

  private getUserTopics(): void {
    this.topicsService.getTopics().subscribe(
        (response: TopicModelIn[]) => {
          this.topics = response;
        },
        error => {
          console.log('POST call in error modified', error);
        },
        () => {
          console.log('The POST observable is now completed.');
        });
  }
}
