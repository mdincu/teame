import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  constructor(private httpClient: HttpClient) {
  }

  getCategories(): Observable<Object> {
    const headers = new HttpHeaders({'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Origin', 'enctype': 'multipart/form-data'});

    const categories = this.httpClient
      .get('http://localhost:8080/category', {headers: headers});

    return categories;
  }
}
