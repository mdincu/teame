import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  @Input() identifier: string;
  @Input() label: string;
  @Input() url: string;

  constructor(private router: Router) { }

  ngOnInit() {
  }

}
