import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SubjectsService {

  constructor(private httpClient: HttpClient) {
  }

  getSubjects(): Observable<Object> {
    const headers = new HttpHeaders({'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Origin', 'enctype': 'multipart/form-data'});

    const subjects = this.httpClient
      .get('http://localhost:8080/subject', {headers: headers});

    return subjects;
  }
}
