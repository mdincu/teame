import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-subject',
  templateUrl: './subject.component.html',
  styleUrls: ['./subject.component.css']
})
export class SubjectComponent implements OnInit {

  @Input() identifier: string;
  @Input() label: string;
  @Input() url: string;

  constructor(private router: Router) { }

  ngOnInit() {
  }

}
