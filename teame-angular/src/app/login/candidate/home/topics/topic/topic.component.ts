import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-topic',
  templateUrl: './topic.component.html',
  styleUrls: ['./topic.component.css']
})
export class TopicComponent implements OnInit {

  @Input() identifier: string;
  @Input() label: string;
  @Input() url: string;

  constructor(private router: Router) { }

  ngOnInit() {
  }
}
