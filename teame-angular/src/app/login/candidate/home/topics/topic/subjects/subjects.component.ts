import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SubjectModelIn} from './subject/subject.model';
import {SubjectsService} from './subjects.service';

@Component({
  selector: 'app-subjects',
  templateUrl: './subjects.component.html',
  styleUrls: ['./subjects.component.css']
})
export class SubjectsComponent implements OnInit {

  subjects: SubjectModelIn [];

  constructor(private httpClient: HttpClient, private subjectsService: SubjectsService) {
  }

  ngOnInit() {
    this.getUserSubjects();
  }

  private getUserSubjects(): void {
    this.subjectsService.getSubjects().subscribe(
      (response: SubjectModelIn[]) => {
        this.subjects = response;
      },
      error => {
        console.log('POST call in error modified', error);
      },
      () => {
        console.log('The POST observable is now completed.');
      });
  }
}
