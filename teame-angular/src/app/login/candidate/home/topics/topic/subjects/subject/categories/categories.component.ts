import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SubjectsService} from '../../subjects.service';
import {SubjectModelIn} from '../subject.model';
import {CategoryModelIn} from './category/category.model';
import {CategoriesService} from './categories.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  categories: CategoryModelIn [];

  constructor(private httpClient: HttpClient, private categoriesService: CategoriesService) {
  }

  ngOnInit() {
    this.getUserCategories();
  }

  private getUserCategories(): void {
    this.categoriesService.getCategories().subscribe(
      (response: CategoryModelIn[]) => {
        this.categories = response;
      },
      error => {
        console.log('POST call in error modified', error);
      },
      () => {
        console.log('The POST observable is now completed.');
      });
  }
}
