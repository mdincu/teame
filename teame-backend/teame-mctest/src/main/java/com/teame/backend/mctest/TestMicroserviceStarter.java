package com.teame.backend.mctest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestMicroserviceStarter {
    public static void main(String[] args){
        SpringApplication.run(TestMicroserviceStarter.class, args);
    }
}
