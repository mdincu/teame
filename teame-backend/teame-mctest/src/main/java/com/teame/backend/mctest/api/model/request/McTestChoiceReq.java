package com.teame.backend.mctest.api.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class McTestChoiceReq {

    @JsonProperty
    private Long identifier;

    @JsonProperty
    private String description;

    public McTestChoiceReq() {
    }

    public McTestChoiceReq(Long identifier, String description) {
        this.identifier = identifier;
        this.description = description;
    }

    public Long getIdentifier() {
        return identifier;
    }

    public void setIdentifier(Long identifier) {
        this.identifier = identifier;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
