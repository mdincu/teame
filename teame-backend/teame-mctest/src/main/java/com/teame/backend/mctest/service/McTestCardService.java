package com.teame.backend.mctest.service;

import com.teame.backend.mctest.entity.McTestSubject;
import com.teame.backend.mctest.repository.McTestCardRepository;
import com.teame.backend.mctest.api.model.request.McTestSubjectReq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class McTestCardService {

    private final McTestCardRepository repository;

    @Autowired
    public McTestCardService(McTestCardRepository repository) {
        this.repository = repository;
    }

    public List<McTestSubjectReq> getUserMcTests(List<String> identifiers){
        return getMcTestCardsByIdentifiers(identifiers)
                .stream()
                .map(mcTestCard -> createMcTestCardBean(mcTestCard))
                .collect(Collectors.toList());
    }

    private List<McTestSubject> getMcTestCardsByIdentifiers(List<String> identifiers){
        return repository.findByIdentifierIn(identifiers);
    }

    private McTestSubjectReq createMcTestCardBean(McTestSubject mcTestSubject) {
        return new McTestSubjectReq(
                mcTestSubject.getIdentifier(),
                mcTestSubject.getDescription(),
                mcTestSubject.getImageUrl(),
                mcTestSubject.getTitle()
        );
    }
}
