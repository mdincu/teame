package com.teame.backend.mctest.repository;

import com.teame.backend.mctest.entity.McTestSubject;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface McTestCardRepository extends CrudRepository<McTestSubject, Integer> {

    McTestSubject findByIdentifier(String identifier);

    List<McTestSubject> findByIdentifierIn(List<String> identifiers);

}
