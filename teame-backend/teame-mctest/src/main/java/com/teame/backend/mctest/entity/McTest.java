package com.teame.backend.mctest.entity;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "MC_TEST")
public class McTest {

	@Id
	@GeneratedValue(generator = "mctest_identity_gen")
	@GenericGenerator(name = "mctest_identity_gen", strategy = "identity")
    private Long identifier;

    @OneToOne
    private McTestSubject mcTestSubject;

    @OneToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinColumn(name = "MC_TEST_USER_ANSWER")
    private List<McTestAnswer> answers;

    private double score;

    public McTest() {
    }

    public Long getIdentifier() {
        return identifier;
    }

    public McTestSubject getMcTestSubject() {
        return mcTestSubject;
    }

    public void setMcTestSubject(McTestSubject mcTestSubject) {
        this.mcTestSubject = mcTestSubject;
    }

    public List<McTestAnswer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<McTestAnswer> answers) {
        this.answers = answers;
    }

    public void setIdentifier(Long identifier) {
        this.identifier = identifier;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }
}
