package com.teame.backend.mctest.repository;

import com.teame.backend.mctest.entity.McTest;
import org.springframework.data.repository.CrudRepository;

public interface McTestRepository extends CrudRepository<McTest, Integer> {

    McTest findByIdentifier(Long identifier);

}
