package com.teame.backend.mctest.entity;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "MC_TEST_QUESTION")
public class McTestQuestion {

	@Id
	@GeneratedValue(generator = "mctestquestion_identity_gen")
	@GenericGenerator(name = "mctestquestion_identity_gen", strategy = "identity")
    protected Long identifier;

    protected String description;

    @OneToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinColumn(name = "MC_TEST_QUESTION_CHOICE")
    protected List<McTestChoice> choices;

    @OneToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinColumn(name = "MC_TEST_QUESTION_CORRECT_CHOICE")
    protected List<McTestChoice> correctChoices;

    @OneToOne
    protected McTestSubject mcTestSubject;

    public McTestQuestion() {
    }

    public McTestQuestion(String description, List<McTestChoice> choices,
                          List<McTestChoice> correctChoices, McTestSubject mcTestSubject) {
        this.description = description;
        this.choices = choices;
        this.correctChoices = correctChoices;
        this.mcTestSubject = mcTestSubject;
    }

    public Long getIdentifier() {
        return identifier;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<McTestChoice> getChoices() {
        return choices;
    }

    public void setChoices(List<McTestChoice> choices) {
        this.choices = choices;
    }

    public List<McTestChoice> getCorrectChoices() {
        return correctChoices;
    }

    public void setCorrectChoices(List<McTestChoice> correctChoices) {
        this.correctChoices = correctChoices;
    }

    public McTestSubject getMcTestSubject() {
        return mcTestSubject;
    }

    public void setMcTestSubject(McTestSubject mcTestSubject) {
        this.mcTestSubject = mcTestSubject;
    }
}
