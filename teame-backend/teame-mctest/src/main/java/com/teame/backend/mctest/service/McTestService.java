package com.teame.backend.mctest.service;

import com.teame.backend.mctest.entity.*;
import com.teame.backend.mctest.repository.McTestCardRepository;
import com.teame.backend.mctest.repository.McTestRepository;
import com.teame.backend.mctest.api.model.request.McTestChoiceReq;
import com.teame.backend.mctest.api.model.request.McTestReq;
import com.teame.backend.mctest.api.model.request.McTestQuestionReq;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class McTestService {

    private final McTestCardRepository mcTestCardRepository;
    private final McTestRepository mcTestRepository;

    @PersistenceContext
    private EntityManager entityManager;

    public McTestService(McTestCardRepository mcTestCardRepository, McTestRepository mcTestRepository) {
        this.mcTestCardRepository = mcTestCardRepository;
        this.mcTestRepository = mcTestRepository;
    }

    public McTestReq createMcTestByTestCardIdentifier(String identifier) {
        Criteria criteria = getCriteria();

        List<Integer> randomRows = getRandomRows(criteria);

        List<McTestQuestion> questionList = getRandomMcTestQuestions(criteria, identifier, randomRows);

        List<McTestAnswer> mcTestAnswerList = createMcTestAnswers(questionList);

        McTestSubject mcTestSubject = getMcTestCard(identifier);

        McTest mcTest = createMcTest(mcTestSubject, mcTestAnswerList);

        return convertMcTestModel(mcTest);
    }

    private List<McTestAnswer> createMcTestAnswers(List<McTestQuestion> questionList) {
        return questionList.stream().map(mcTestQuestion -> new McTestAnswer(Collections.emptyList(), mcTestQuestion)).collect(Collectors.toList());
    }

    public double submitTest(McTestReq mcTestReq) {
        Long identifier = mcTestReq.getIdentifier();

        McTest mcTest = mcTestRepository.findByIdentifier(identifier);
        List<McTestAnswer> answers = mcTest.getAnswers();
        mcTestReq
                .getQuestions()
                .stream()
                .forEach(mcTestQuestionReq -> {
                    List<McTestChoiceReq> userChoicesModelList = mcTestQuestionReq.getUserChoices();
                    List<McTestChoice> userChoicesList = userChoicesModelList
                            .stream()
                            .map(mcTestChoiceReq -> new McTestChoice(mcTestChoiceReq.getIdentifier(), mcTestChoiceReq.getDescription()))
                            .collect(Collectors.toList());
                    answers
                            .stream()
                            .forEach(mcTestAnswer -> {
                                if (mcTestAnswer.getQuestion().getIdentifier().equals(mcTestQuestionReq.getIdentifier())) {
                                    mcTestAnswer.setUserChoices(userChoicesList);
                                }
                            });
                });

        double score = calculateScore(mcTest);
        mcTest.setScore(score);

        mcTestRepository.save(mcTest);

        return score;
    }

    private double calculateScore(McTest mcTest) {
        return (double) mcTest
                .getAnswers()
                .stream()
                .filter(mcTestAnswer -> {
                    List<McTestChoice> correctChoices = new ArrayList<>(mcTestAnswer.getQuestion().getCorrectChoices());
                    return correctChoices.equals(mcTestAnswer.getUserChoices());
                })
                .count() / mcTest.getAnswers().size();
    }

    private McTest createMcTest(McTestSubject mcTestSubject, List<McTestAnswer> mcTestAnswerList) {
        McTest mcTest = new McTest();
        mcTest.setMcTestSubject(mcTestSubject);
        mcTest.setAnswers(mcTestAnswerList);
        return mcTestRepository.save(mcTest);
    }

    private Criteria getCriteria() {
        return entityManager
                .unwrap(Session.class)
                .createCriteria(McTestQuestion.class);
    }

    private List<Integer> getRandomRows(Criteria criteria) {
        int totalRowsNumber = ((Number) criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
        criteria.setProjection(null); // nu mai merge daca nu e null
        List<Integer> rows = IntStream
                .rangeClosed(0, totalRowsNumber - 1)
                .boxed()
                .collect(Collectors.toList());
        Collections.shuffle(rows);
        return rows;
    }

    private List<McTestQuestion> getRandomMcTestQuestions(Criteria criteria, String identifier, List<Integer> randomRows) {
        criteria.setMaxResults(1);

        return randomRows.stream()
                .map(randomRow -> (McTestQuestion) criteria
                        .setFirstResult(randomRow)
                        .uniqueResult())
                .filter(mcTestQuestion -> identifier.equals(mcTestQuestion.getMcTestSubject().getIdentifier()))
                .limit(McTestReq.NR_QUESTIONS)
                .collect(Collectors.toList());
    }

    private McTestSubject getMcTestCard(String identifier) {
        return mcTestCardRepository.findByIdentifier(identifier);
    }

    private McTestReq convertMcTestModel(McTest mcTest) {
        return new McTestReq(
                mcTest.getIdentifier(),
                mcTest.getMcTestSubject().getTitle(),
                mcTest.getMcTestSubject().getDescription(),
                mcTest.getAnswers()
                        .stream()
                        .map(mcTestAnswer -> createMcTestQuestionBean(mcTestAnswer.getQuestion()))
                        .collect(Collectors.toList())
        );
    }

    private McTestQuestionReq createMcTestQuestionBean(McTestQuestion mcTestQuestion) {
        return new McTestQuestionReq(
                mcTestQuestion.getIdentifier(),
                mcTestQuestion.getDescription(),
                mcTestQuestion
                        .getChoices()
                        .stream()
                        .map(mcTestChoice -> createMcTestAnswerBean(mcTestChoice))
                        .collect(Collectors.toList()),
                Collections.emptyList());
    }

    private McTestChoiceReq createMcTestAnswerBean(McTestChoice mcTestChoice) {
        return new McTestChoiceReq(
                mcTestChoice.getIdentifier(),
                mcTestChoice.getDescription()
        );
    }
}
