package com.teame.backend.mctest.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name="MC_TEST_CARD")
public class McTestSubject {

    @Id
    @GeneratedValue(generator = "mctestsubject_identity_gen")
    @GenericGenerator(name = "mctestsubject_identity_gen", strategy = "identity")
    private Long identifier;

    private String title;

    private String description;

    private String imageUrl;

    public McTestSubject() {
    }

    public McTestSubject(String title, String description, String imageUrl) {
        this.title = title;
        this.description = description;
        this.imageUrl = imageUrl;
    }

    public Long getIdentifier() {
        return identifier;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
