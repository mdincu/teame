package com.teame.backend.mctest.utils;

import com.teame.backend.mctest.entity.McTestSubject;
import com.teame.backend.mctest.entity.McTestChoice;
import com.teame.backend.mctest.entity.McTestQuestion;
import com.teame.backend.mctest.repository.McTestCardRepository;
import com.teame.backend.mctest.repository.McTestQuestionRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class McTestQuestionListGeneratorService {

    private final McTestQuestionRepository mcTestQuestionRepository;
    private final McTestCardRepository mcTestCardRepository;

    public McTestQuestionListGeneratorService(McTestQuestionRepository mcTestQuestionRepository, McTestCardRepository mcTestCardRepository) {
        this.mcTestQuestionRepository = mcTestQuestionRepository;
        this.mcTestCardRepository = mcTestCardRepository;
    }

    public void insertQuestionsInDB(){

        McTestSubject mcTestSubjectJava = createMcTestCard("Java course card title", "Java course card description", "Java course card imageUrl");

        mcTestCardRepository.save(mcTestSubjectJava);
        for (int i = 0; i < 15; i++) {
            mcTestQuestionRepository.save(createMcTestQuestion("Java course question " + i, mcTestSubjectJava));
        }

        McTestSubject mcTestSubjectCSharp = createMcTestCard("C# course card title", "C# course card description", "C# course card imageUrl");
        mcTestCardRepository.save(mcTestSubjectCSharp);
        for (int i = 0; i < 15; i++) {
            mcTestQuestionRepository.save(createMcTestQuestion("C# course question " + i, mcTestSubjectCSharp));
        }
    }

    private McTestSubject createMcTestCard(String title, String description, String imageUrl) {
        McTestSubject mcTestSubject = new McTestSubject();
        mcTestSubject.setTitle(title);
        mcTestSubject.setDescription(description);
        mcTestSubject.setImageUrl(imageUrl);
        return mcTestSubject;
    }

    private McTestQuestion createMcTestQuestion(String description, McTestSubject mcTestSubject) {
        McTestQuestion mcTestQuestion = new McTestQuestion();
        mcTestQuestion.setDescription(description);
        McTestChoice choice1 = createMcTestChoice("one");
        McTestChoice choice2 = createMcTestChoice("two");
        McTestChoice choice3 = createMcTestChoice("three");
        McTestChoice choice4 = createMcTestChoice("four");
        mcTestQuestion.setChoices(createMcTestChoiceList(choice1, choice2, choice3, choice4));
        mcTestQuestion.setCorrectChoices(createMcTestChoiceList(choice2, choice3));
        mcTestQuestion.setMcTestSubject(mcTestSubject);
        return mcTestQuestion;
    }

    private List<McTestChoice> createMcTestChoiceList(McTestChoice... choices) {
        List<McTestChoice> mcTestChoiceList = new ArrayList<>();
        for (McTestChoice choice : choices) {
            mcTestChoiceList.add(choice);
        }
        return mcTestChoiceList;
    }

    private McTestChoice createMcTestChoice(String description) {
        McTestChoice mcTestChoice = new McTestChoice();
        mcTestChoice.setDescription(description);
        return mcTestChoice;
    }
}
