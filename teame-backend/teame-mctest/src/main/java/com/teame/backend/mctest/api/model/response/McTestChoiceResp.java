package com.teame.backend.mctest.api.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class McTestChoiceResp {

    @JsonProperty
    private Long identifier;

    @JsonProperty
    private String description;

    public McTestChoiceResp() {
    }

    public McTestChoiceResp(Long identifier, String description) {
        this.identifier = identifier;
        this.description = description;
    }

    public Long getIdentifier() {
        return identifier;
    }

    public void setIdentifier(Long identifier) {
        this.identifier = identifier;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
