package com.teame.backend.mctest.entity;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "MC_TEST_ANSWER")
public class McTestAnswer {

	@Id
	@GeneratedValue(generator = "mctestanswer_identity_gen")
	@GenericGenerator(name = "mctestanswer_identity_gen", strategy = "identity")
    protected Long identifier;

    @OneToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinColumn(name = "MC_TEST_ANSWER_USER_CHOICE")
    private List<McTestChoice> userChoices;

    @OneToOne
    private McTestQuestion question;

    public McTestAnswer() {
    }

    public McTestAnswer(List<McTestChoice> userChoices, McTestQuestion question) {
        this.userChoices = userChoices;
        this.question = question;
    }

    public List<McTestChoice> getUserChoices() {
        return userChoices;
    }

    public void setUserChoices(List<McTestChoice> userChoices) {
        this.userChoices = userChoices;
    }

    public McTestQuestion getQuestion() {
        return question;
    }

    public void setQuestion(McTestQuestion question) {
        this.question = question;
    }
}
