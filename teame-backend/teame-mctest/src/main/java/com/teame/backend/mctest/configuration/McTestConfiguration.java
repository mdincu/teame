package com.teame.backend.mctest.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import static com.teame.backend.mctest.configuration.Constants.SCAN_PACKAGE;

@Configuration
@ComponentScan(SCAN_PACKAGE)
@EnableJpaRepositories(SCAN_PACKAGE)
public class McTestConfiguration {
}
