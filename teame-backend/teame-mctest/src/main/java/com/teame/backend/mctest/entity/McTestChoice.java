package com.teame.backend.mctest.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "MC_TEST_CHOICE")
public class McTestChoice {

	@Id
	@GeneratedValue(generator = "mctestchoice_identity_gen")
	@GenericGenerator(name = "mctestchoice_identity_gen", strategy = "identity")
    private Long identifier;

    private String description;

    public McTestChoice() {
    }

    public McTestChoice(Long identifier, String description) {
        this.identifier = identifier;
        this.description = description;
    }

    public McTestChoice(String description) {
        this.description = description;
    }

    public Long getIdentifier() {
        return identifier;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof McTestChoice)) return false;
        McTestChoice that = (McTestChoice) o;
        return Objects.equals(identifier, that.identifier);
    }

    @Override
    public int hashCode() {
        return Objects.hash(identifier);
    }
}
