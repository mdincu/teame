package com.teame.backend.mctest.api.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class McTestSubjectResp {

    @JsonProperty
    private Long identifier;

    @JsonProperty
    private String description;

    @JsonProperty
    private String title;

    @JsonProperty
    private String imageUrl;


    public McTestSubjectResp() {
    }

    public McTestSubjectResp(Long identifier, String description, String title, String imageUrl) {
        this.identifier = identifier;
        this.description = description;
        this.title = title;
        this.imageUrl = imageUrl;
    }

    public Long getIdentifier() {
        return identifier;
    }

    public void setIdentifier(Long identifier) {
        this.identifier = identifier;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
