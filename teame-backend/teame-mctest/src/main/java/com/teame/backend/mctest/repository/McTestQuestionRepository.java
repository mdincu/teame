package com.teame.backend.mctest.repository;

import com.teame.backend.mctest.entity.McTestQuestion;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface McTestQuestionRepository extends CrudRepository<McTestQuestion, Integer> {

    @Query("SELECT COUNT(*) FROM McTestQuestion")
    int getNumberOfRows();

}
