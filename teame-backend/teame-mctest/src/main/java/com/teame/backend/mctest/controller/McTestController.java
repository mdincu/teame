package com.teame.backend.mctest.controller;

import com.teame.backend.mctest.service.McTestService;
import com.teame.backend.mctest.utils.McTestGeneratorService;
import com.teame.backend.mctest.utils.McTestQuestionListGeneratorService;
import com.teame.backend.mctest.api.model.request.McTestReq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/mctest")
public class McTestController {

    private final McTestService mcTestService;
    private final McTestQuestionListGeneratorService mcTestQuestionListGeneratorService;
    private final McTestGeneratorService mcTestGeneratorService;

    @Autowired
    public McTestController(McTestService mcTestService, McTestQuestionListGeneratorService mcTestQuestionListGeneratorService, McTestGeneratorService mcTestGeneratorService) {
        this.mcTestService = mcTestService;
        this.mcTestQuestionListGeneratorService = mcTestQuestionListGeneratorService;
        this.mcTestGeneratorService = mcTestGeneratorService;
    }

    @RequestMapping(value = "/subject", method = RequestMethod.POST)
    public ResponseEntity<McTestReq> byMcTestSubjectIdentifier(@RequestParam("identifier") String mcTestSubjectIdentifier) {
        return new ResponseEntity<>(mcTestService.createMcTestByTestCardIdentifier(mcTestSubjectIdentifier), HttpStatus.OK);
    }

    @RequestMapping(value = "/submit", method = RequestMethod.POST)
    public ResponseEntity<Double> submit(@RequestBody McTestReq mcTestReq) {
        double score = mcTestService.submitTest(mcTestReq);
        return new ResponseEntity<>(score, HttpStatus.OK);
    }
}
