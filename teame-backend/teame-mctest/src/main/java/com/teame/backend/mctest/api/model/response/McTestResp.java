package com.teame.backend.mctest.api.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.teame.backend.mctest.api.model.request.McTestQuestionReq;

import java.util.List;

public class McTestResp {

    @JsonProperty
    private Long identifier;

    @JsonProperty
    private String name;

    @JsonProperty
    private String description;

    @JsonProperty
    private List<McTestQuestionReq> questions;

    public static final int NR_QUESTIONS = 10;


    public McTestResp() {
    }

    public McTestResp(Long identifier, String name, String description, List<McTestQuestionReq> questions) {
        this.identifier = identifier;
        this.name = name;
        this.description = description;
        this.questions = questions;
    }

    public Long getIdentifier() {
        return identifier;
    }

    public void setIdentifier(Long identifier) {
        this.identifier = identifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<McTestQuestionReq> getQuestions() {
        return questions;
    }

    public void setQuestions(List<McTestQuestionReq> questions) {
        this.questions = questions;
    }
}
