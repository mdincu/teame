package com.teame.backend.mctest.controller;

import com.teame.backend.mctest.service.McTestCardService;
import com.teame.backend.mctest.utils.McTestQuestionListGeneratorService;
import com.teame.backend.mctest.api.model.request.McTestQuestionReq;
import com.teame.backend.mctest.api.model.request.McTestSubjectReq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/mctestsubject")
public class McTestSubjectController {

    private final McTestCardService mcTestCardService;
    private final McTestQuestionListGeneratorService mcTestQuestionListGeneratorService;

    @Autowired
    public McTestSubjectController(McTestCardService mcTestCardService, McTestQuestionListGeneratorService mcTestQuestionListGeneratorService) {
        this.mcTestCardService = mcTestCardService;
        this.mcTestQuestionListGeneratorService = mcTestQuestionListGeneratorService;
    }

    @RequestMapping(value = "/identifiers", method = RequestMethod.POST)
    public ResponseEntity<List<McTestSubjectReq>> byIdentifiers(@RequestBody List<String> identifiers) {
        return new ResponseEntity<>(mcTestCardService.getUserMcTests(identifiers), HttpStatus.OK);
    }

    @RequestMapping(value = "/add-questions", method = RequestMethod.GET)
    public ResponseEntity<Boolean> addQuestion() {
        mcTestQuestionListGeneratorService.insertQuestionsInDB();
        return new ResponseEntity<>(true, HttpStatus.OK);
    }

    @RequestMapping(value = "/question/create", method = RequestMethod.POST)
    public ResponseEntity<Boolean> createNewQuestion(@RequestBody McTestQuestionReq mcTestQuestionReq) {
        return new ResponseEntity<>(true, HttpStatus.OK);
    }
}
