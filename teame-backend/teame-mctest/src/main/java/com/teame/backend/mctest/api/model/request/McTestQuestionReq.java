package com.teame.backend.mctest.api.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class McTestQuestionReq {

    @JsonProperty
    private Long identifier;

    @JsonProperty
    private String description;

    @JsonProperty
    private List<McTestChoiceReq> choices;

    @JsonProperty
    private List<McTestChoiceReq> userChoices;

    public McTestQuestionReq() {
    }

    public McTestQuestionReq(Long identifier, String description, List<McTestChoiceReq> answers, List<McTestChoiceReq> userChoices) {
        this.identifier = identifier;
        this.description = description;
        this.choices = answers;
        this.userChoices = userChoices;
    }

    public Long getIdentifier() {
        return identifier;
    }

    public void setIdentifier(Long identifier) {
        this.identifier = identifier;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<McTestChoiceReq> getAnswers() {
        return choices;
    }

    public void setAnswers(List<McTestChoiceReq> answers) {
        this.choices = answers;
    }

    public List<McTestChoiceReq> getUserChoices() {
        return userChoices;
    }

    public void setUserChoices(List<McTestChoiceReq> userChoices) {
        this.userChoices = userChoices;
    }

    public List<McTestChoiceReq> getChoices() {
        return choices;
    }

    public void setChoices(List<McTestChoiceReq> choices) {
        this.choices = choices;
    }
}
