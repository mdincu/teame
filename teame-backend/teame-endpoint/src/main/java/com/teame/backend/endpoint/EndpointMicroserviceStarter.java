package com.teame.backend.endpoint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EndpointMicroserviceStarter {

    public static void main(String[] args){
        SpringApplication.run(EndpointMicroserviceStarter.class, args);
    }

}
