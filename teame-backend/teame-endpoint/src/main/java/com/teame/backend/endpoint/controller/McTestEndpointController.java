package com.teame.backend.endpoint.controller;

import com.teame.backend.mctest.api.model.request.McTestReq;
import com.teame.backend.mctest.api.model.response.McTestResp;
import com.teame.backend.mctest.api.model.response.McTestSubjectResp;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(value = "/mctest")
public class McTestEndpointController {

    private HttpHeaders headers;

    private UriComponentsBuilder builder;

    private RestTemplate restTemplate;

    McTestEndpointController() {
        this.headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        this.restTemplate = new RestTemplate();
    }

    @RequestMapping(value = "/mctestsubjects", method = RequestMethod.GET)
    public ResponseEntity<List<McTestSubjectResp>> getMcTestSubjects(Principal principal) {

        final List<String> mcTestCardsIdentifiers;
        final List<McTestSubjectResp> mcTestSubjectExternalIns;

        builder = UriComponentsBuilder
                .fromHttpUrl("http://localhost:8082/user/mctest/subjects/")
                .path(principal.getName());

        HttpEntity<?> entity = new HttpEntity<>(headers);

        HttpEntity<List> responseUser = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                entity,
                List.class);

        mcTestCardsIdentifiers = responseUser.getBody();

        builder = UriComponentsBuilder
                .fromHttpUrl("http://localhost:8085/mctestsubject/identifiers");

        HttpEntity<List<String>> mcTestCardsIdentifiersRequest = new HttpEntity<>(mcTestCardsIdentifiers, headers);

        HttpEntity<List> responseTest = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.POST,
                mcTestCardsIdentifiersRequest,
                List.class);

        mcTestSubjectExternalIns = responseTest.getBody();

        return new ResponseEntity<>(mcTestSubjectExternalIns, HttpStatus.OK);

    }

    @RequestMapping(value = "/subject", method = RequestMethod.POST)
    public ResponseEntity<McTestResp> getNewTest(@RequestParam("identifier") String mcTestCardIdentifier) {

        McTestResp mcTest = new McTestResp();

        builder = UriComponentsBuilder.fromHttpUrl("http://localhost:8085/mctest/subject")
                .queryParam("identifier", mcTestCardIdentifier);

        HttpEntity<String> mcTestCardIdentifierRequest = new HttpEntity<>(headers);

        HttpEntity<McTestResp> responseTest = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.POST,
                mcTestCardIdentifierRequest,
                McTestResp.class);

        mcTest = responseTest.getBody();

        return new ResponseEntity<>(mcTest, HttpStatus.OK);
    }

    @RequestMapping(value = "/submit", method = RequestMethod.POST)
    public ResponseEntity<Boolean> submit(@RequestBody McTestReq mcTestExternalIn) {

        builder = UriComponentsBuilder
                .fromHttpUrl("http://localhost:8085/mctest/submit/");

        HttpEntity<McTestReq> httpEntity = new HttpEntity<>(mcTestExternalIn, headers);

        HttpEntity<Boolean> responseTest = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.POST,
                httpEntity,
                Boolean.class);

        return new ResponseEntity(responseTest.getBody(), HttpStatus.OK);
    }
}