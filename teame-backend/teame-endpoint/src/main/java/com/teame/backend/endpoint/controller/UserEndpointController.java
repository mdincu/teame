package com.teame.backend.endpoint.controller;

import com.teame.backend.endpoint.model.api.SignupRequest;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
public class UserEndpointController {

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public ResponseEntity<Boolean> signup(@RequestBody SignupRequest signupRequest) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("http://localhost:8082/user/signup")
                .queryParam("email", signupRequest.getEmail())
                .queryParam("password", signupRequest.getPassword());

        HttpEntity<?> entity = new HttpEntity<>(headers);

        HttpEntity<String> response = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.POST,
                entity,
                String.class);

        return new ResponseEntity<>(new Boolean(response.getBody()), HttpStatus.OK);
    }
}
