package com.teame.backend.endpoint.controller;

import com.teame.backend.endpoint.service.SubjectService;
import com.teame.core.beans.topic.model.external.out.SubjectExternalOut;
import com.teame.core.beans.topic.model.internal.out.SubjectInternalOut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(value = "/subject")
public class SubjectEndpointController {

    private HttpHeaders headers;

    private UriComponentsBuilder builder;

    private RestTemplate restTemplate;

    private final SubjectService subjectService;

    @Autowired
    SubjectEndpointController(SubjectService subjectService) {
        this.subjectService = subjectService;
        this.headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        this.restTemplate = new RestTemplate();
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<SubjectExternalOut>> getSubjectsByUser(Principal principal) {

        final List<String> subjectIdentifiers;
        final List<SubjectInternalOut> subjectInternalOuts;
        final List<SubjectExternalOut> subjectExternalOuts;

        builder = UriComponentsBuilder
                .fromHttpUrl("http://localhost:8082/user/subject/")
                .path(principal.getName());

        HttpEntity<?> entity = new HttpEntity<>(headers);

        HttpEntity<List> responseUser = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                entity,
                List.class);

        subjectIdentifiers = responseUser.getBody();

        builder = UriComponentsBuilder
                .fromHttpUrl("http://localhost:8086/subject");

        HttpEntity<List<String>> subjectIdentifiersRequest = new HttpEntity<>(subjectIdentifiers, headers);

        HttpEntity<SubjectInternalOut.SubjectInternalOutList> responseTest = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.POST,
                subjectIdentifiersRequest,
                SubjectInternalOut.SubjectInternalOutList.class);

        subjectInternalOuts = responseTest.getBody().getSubjectInternalOuts();

        return new ResponseEntity<>(subjectService.convertSubjectInternalOutList2SubjectExternalOutList(subjectInternalOuts),HttpStatus.OK);
    }
}