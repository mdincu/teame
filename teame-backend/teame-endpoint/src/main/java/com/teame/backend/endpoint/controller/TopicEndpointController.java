package com.teame.backend.endpoint.controller;

import com.teame.backend.endpoint.service.TopicService;
import com.teame.core.beans.topic.model.external.out.TopicExternalOut;
import com.teame.core.beans.topic.model.internal.out.TopicInternalOut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(value = "/topic")
public class TopicEndpointController {

    private HttpHeaders headers;

    private HttpEntity<?> entity = new HttpEntity<>(headers);

    private UriComponentsBuilder builder;

    private RestTemplate restTemplate;

    private final TopicService topicService;

    @Autowired
    TopicEndpointController(TopicService topicService) {
        this.topicService = topicService;
        this.headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        this.restTemplate = new RestTemplate();
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<TopicExternalOut>> getTopicsByUser(Principal principal) {

        final List<String> topicIdentifiers;
        final List<TopicInternalOut> topicInternalOuts;
        final List<TopicExternalOut> topicExternalOuts;

        builder = UriComponentsBuilder
                .fromHttpUrl("http://localhost:8082/user/email/")
                .path(principal.getName());

        HttpEntity<String> userResponse = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                entity,
                String.class);

        String userIdentifier = userResponse.getBody();

        builder = UriComponentsBuilder
                .fromHttpUrl("http://localhost:8086/topic/user/")
                .path(userIdentifier);

        entity = new HttpEntity<>(headers);

        HttpEntity<TopicInternalOut.TopicInternalOutList> topicResponse = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                entity,
                TopicInternalOut.TopicInternalOutList.class);

        topicInternalOuts = topicResponse.getBody().getTopicInternalOuts();

        return new ResponseEntity<>(topicService.convertTopicInternalOutList2TopicExternalOutList(topicInternalOuts),HttpStatus.OK);
    }
}