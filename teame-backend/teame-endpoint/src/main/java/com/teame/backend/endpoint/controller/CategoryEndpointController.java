package com.teame.backend.endpoint.controller;

import com.teame.backend.endpoint.service.CategoryService;
import com.teame.core.beans.topic.model.external.out.CategoryExternalOut;
import com.teame.core.beans.topic.model.internal.out.CategoryInternalOut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(value = "/category")
public class CategoryEndpointController {

    private HttpHeaders headers;

    private UriComponentsBuilder builder;

    private RestTemplate restTemplate;

    private final CategoryService categoryService;

    @Autowired
    CategoryEndpointController(CategoryService categoryService) {
        this.categoryService = categoryService;
        this.headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        this.restTemplate = new RestTemplate();
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<CategoryExternalOut>> getCategorysByUser(Principal principal) {

        final List<String> categoryIdentifiers;
        final List<CategoryInternalOut> categoryInternalOuts;
        final List<CategoryExternalOut> categoryExternalOuts;

        builder = UriComponentsBuilder
                .fromHttpUrl("http://localhost:8082/user/category/")
                .path(principal.getName());

        HttpEntity<?> entity = new HttpEntity<>(headers);

        HttpEntity<List> responseUser = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                entity,
                List.class);

        categoryIdentifiers = responseUser.getBody();

        builder = UriComponentsBuilder
                .fromHttpUrl("http://localhost:8086/category");

        HttpEntity<List<String>> categoryIdentifiersRequest = new HttpEntity<>(categoryIdentifiers, headers);

        HttpEntity<CategoryInternalOut.CategoryInternalOutList> responseTest = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.POST,
                categoryIdentifiersRequest,
                CategoryInternalOut.CategoryInternalOutList.class);

        categoryInternalOuts = responseTest.getBody().getCategoryInternalOuts();

        return new ResponseEntity<>(categoryService.convertCategoryInternalOutList2CategoryExternalOutList(categoryInternalOuts), HttpStatus.OK);
    }
}