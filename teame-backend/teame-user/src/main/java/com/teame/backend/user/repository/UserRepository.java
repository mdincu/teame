package com.teame.backend.user.repository;


import com.teame.backend.user.entity.Candidate;
import com.teame.backend.user.entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Integer> {

    User findByEmail(String email);

}
