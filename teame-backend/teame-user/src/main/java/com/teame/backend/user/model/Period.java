package com.teame.backend.user.model;

import java.time.Month;
import java.time.Year;

public class Period {
    Month fromMonth;
    Year fromYear;
    Month toMonth;
    Year toYear;

    public Period(Month fromMonth, Year fromYear, Month toMonth, Year toYear) {
        this.fromMonth = fromMonth;
        this.fromYear = fromYear;
        this.toMonth = toMonth;
        this.toYear = toYear;
    }

    public Month getFromMonth() {
        return fromMonth;
    }

    public void setFromMonth(Month fromMonth) {
        this.fromMonth = fromMonth;
    }

    public Year getFromYear() {
        return fromYear;
    }

    public void setFromYear(Year fromYear) {
        this.fromYear = fromYear;
    }

    public Month getToMonth() {
        return toMonth;
    }

    public void setToMonth(Month toMonth) {
        this.toMonth = toMonth;
    }

    public Year getToYear() {
        return toYear;
    }

    public void setToYear(Year toYear) {
        this.toYear = toYear;
    }
}
