package com.teame.backend.user.repository;

import com.teame.backend.user.entity.Candidate;
import org.springframework.data.repository.CrudRepository;

public interface CandidateRepository extends CrudRepository<Candidate, Integer> {

    Candidate findByEmail(String email);

}
