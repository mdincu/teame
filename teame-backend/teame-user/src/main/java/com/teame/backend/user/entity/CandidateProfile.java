package com.teame.backend.user.entity;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "CANDIDATE_PROFILE")
public class CandidateProfile extends Profile{

    @Id
    @Column(name = "PROFILE_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "CURRENT_COMPANY")
    private String currentCompany;

    @OneToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinColumn(name = "EXPERIENCE")
    List<Experience> experienceList;

    @OneToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinColumn(name = "EDUCATION")
    List<Education> educationList;

    @OneToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinColumn(name = "ACCOMPLISHMENT")
    List<Accomplishment> accomplishmentList;

    public CandidateProfile() {
    }

    public CandidateProfile(String currentCompany, List<Experience> experienceList,
                            List<Education> educationList, List<Accomplishment> accomplishmentList) {
        this.currentCompany = currentCompany;
        this.experienceList = experienceList;
        this.educationList = educationList;
        this.accomplishmentList = accomplishmentList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCurrentCompany() {
        return currentCompany;
    }

    public void setCurrentCompany(String currentCompany) {
        this.currentCompany = currentCompany;
    }

    public List<Experience> getExperienceList() {
        return experienceList;
    }

    public void setExperienceList(List<Experience> experienceList) {
        this.experienceList = experienceList;
    }

    public List<Education> getEducationList() {
        return educationList;
    }

    public void setEducationList(List<Education> educationList) {
        this.educationList = educationList;
    }

    public List<Accomplishment> getAccomplishmentList() {
        return accomplishmentList;
    }

    public void setAccomplishmentList(List<Accomplishment> accomplishmentList) {
        this.accomplishmentList = accomplishmentList;
    }

}
