package com.teame.backend.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserMicroserviceStarter {
    public static void main(String[] args){
        SpringApplication.run(UserMicroserviceStarter.class, args);
    }
}
