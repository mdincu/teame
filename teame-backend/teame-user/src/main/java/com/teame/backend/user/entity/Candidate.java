package com.teame.backend.user.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "USER_CANDIDATE")
public class Candidate extends User {

    @OneToOne
    @JoinColumn(name = "PROFILE_ID")
    private CandidateProfile profile;

    public Candidate() {
    }

    public Candidate(String email, String password, List<String> roles) {
        super(email, password);
    }
}
