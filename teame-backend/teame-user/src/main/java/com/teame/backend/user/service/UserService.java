package com.teame.backend.user.service;

import com.teame.backend.user.entity.User;
import com.teame.backend.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public boolean save(String email, String password) {
        try {
            userRepository.save(new User(email, password));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean login(String email, String password) {
        User user = userRepository.findByEmail(email);
        return password != null && password.equals(user.getPassword());
    }

    public String getUserIdentifierByEmail(String email) {
        User user = userRepository.findByEmail(email);
        return user.getIdentifier();
    }
}
