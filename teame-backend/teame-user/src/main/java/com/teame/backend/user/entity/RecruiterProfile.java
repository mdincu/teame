package com.teame.backend.user.entity;

import javax.persistence.*;

@Entity
@Table(name = "RECRUITER_PROFILE")
public class RecruiterProfile extends Profile{

    @Id
    @Column(name = "PROFILE_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "NAME")
    private String name;

    public RecruiterProfile() {
    }

    public RecruiterProfile(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
