package com.teame.backend.user.entity;


import javax.persistence.*;

@Entity
@Table(name="ACCOMPLISHMENT")
public class Accomplishment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String title;
    private String description;

    //todo Add media

    public Accomplishment(String title) {
        this.title = title;
    }

    public Accomplishment(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
