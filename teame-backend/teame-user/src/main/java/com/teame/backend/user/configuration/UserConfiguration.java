package com.teame.backend.user.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import static com.teame.backend.user.configuration.Constants.SCAN_PACKAGE;

@Configuration
@ComponentScan(SCAN_PACKAGE)
@EnableJpaRepositories(SCAN_PACKAGE)
public class UserConfiguration {
}
