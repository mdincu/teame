package com.teame.backend.user.controller;

import com.teame.backend.user.service.UserService;
import com.teame.core.beans.topic.model.internal.out.TopicInternalOut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/user")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public ResponseEntity<Boolean> signup(@RequestParam("email") String email,
                                          @RequestParam("password") String password) {
        boolean save = userService.save(email, password);
        return new ResponseEntity<>(save, HttpStatus.OK);
    }

    @RequestMapping(path = "/login", method = RequestMethod.POST)
    public ResponseEntity<Boolean> login(@RequestParam("email") String email,
                                         @RequestParam("password") String password) {
        boolean login = userService.login(email, password);
        return new ResponseEntity<>(login, HttpStatus.OK);
    }

    @RequestMapping(path = "/email/{email}", method = RequestMethod.GET)
    public ResponseEntity<String> getUserIdentifierByEmail(@PathVariable("email") String userEmail) {
        return new ResponseEntity<>(userService.getUserIdentifierByEmail(userEmail), HttpStatus.OK);
    }

//    @RequestMapping(path = "/mctest/subjects/{user}", method = RequestMethod.GET)
//    public ResponseEntity<List<String>> getMcTestSubjectsIdentifiers(@PathVariable("user") String email){
//        return new ResponseEntity<>(userService.getMcTestCardsByEmail(email),HttpStatus.OK);
//    }
}
