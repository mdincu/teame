package com.teame.backend.topic.api.model.request;

import java.util.List;

public class SubjectReq {

    private String identifier;
    private String label;
    private String url;

    public SubjectReq() {
    }

    public SubjectReq(String identifier, String label, String url) {
        this.identifier = identifier;
        this.label = label;
        this.url = url;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public static class SubjectInternalOutList {

        private List<SubjectReq> subjectReqs;

        public SubjectInternalOutList() {
        }

        public SubjectInternalOutList(List<SubjectReq> subjectReqs) {
            this.subjectReqs = subjectReqs;
        }

        public List<SubjectReq> getSubjectReqs() {
            return subjectReqs;
        }

        public void setSubjectReqs(List<SubjectReq> subjectReqs) {
            this.subjectReqs = subjectReqs;
        }
    }
}
