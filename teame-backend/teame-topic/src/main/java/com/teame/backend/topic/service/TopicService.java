package com.teame.backend.topic.service;

import com.teame.backend.topic.entity.Topic;
import com.teame.backend.topic.repository.TopicRepository;
import com.teame.backend.topic.api.model.request.TopicReq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TopicService {

    private final TopicRepository topicRepository;
    private final ConversionService conversionService;

    @Autowired
    public TopicService(TopicRepository topicRepository, ConversionService conversionService) {
        this.topicRepository = topicRepository;
        this.conversionService = conversionService;
    }

    public TopicReq.TopicInternalOutList getTopicsByUserIdentifier(String userIdentifier){
        List<Topic> topics = topicRepository.findByUserIdentifiers(userIdentifier);
        List<TopicReq> topicReqs = new ArrayList<>();
        for(Topic topic : topics){
            topicReqs.add(conversionService.convert(topic, TopicReq.class));
        }
        return new TopicReq.TopicInternalOutList(topicReqs);
    }

}
