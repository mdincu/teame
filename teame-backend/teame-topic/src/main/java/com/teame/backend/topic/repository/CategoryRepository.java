package com.teame.backend.topic.repository;

import com.teame.backend.topic.entity.Category;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CategoryRepository extends CrudRepository<Category, String> {
    List<Category> findByUserIdentifiers(String userIdentifier);
}
