package com.teame.backend.topic.api.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TopicResp {

    @JsonProperty
    private String identifier;
    @JsonProperty
    private String label;

    public TopicResp() {
    }

    public TopicResp(String identifier, String label) {
        this.identifier = identifier;
        this.label = label;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
