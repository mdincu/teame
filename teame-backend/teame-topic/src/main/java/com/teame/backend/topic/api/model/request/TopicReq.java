package com.teame.backend.topic.api.model.request;

import java.util.List;

public class TopicReq {

    private String identifier;
    private String label;

    public TopicReq() {
    }

    public TopicReq(String identifier, String label) {
        this.identifier = identifier;
        this.label = label;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public static class TopicInternalOutList {

        private List<TopicReq> topicReqs;

        public TopicInternalOutList() {
        }

        public TopicInternalOutList(List<TopicReq> topicReqs) {
            this.topicReqs = topicReqs;
        }

        public List<TopicReq> getTopicReqs() {
            return topicReqs;
        }

        public void setTopicReqs(List<TopicReq> topicReqs) {
            this.topicReqs = topicReqs;
        }
    }
}
