package com.teame.backend.topic.converter;

import com.teame.backend.topic.entity.Subject;
import com.teame.backend.topic.api.model.request.SubjectReq;
import org.springframework.core.convert.converter.Converter;

public class Subject2SubjectInternalOutConverter implements Converter<Subject, SubjectReq> {
    @Override
    public SubjectReq convert(Subject subject) {

        return new SubjectReq(subject.getIdentifier(), subject.getLabel(), subject.getUrl());

    }
}
