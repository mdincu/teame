package com.teame.backend.topic.controller;

import com.teame.backend.topic.service.SubjectService;
import com.teame.backend.topic.api.model.request.SubjectReq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RequestMapping("/subject")
public class SubjectController {

    private final SubjectService subjectService;

    @Autowired
    public SubjectController(SubjectService subjectService) {
        this.subjectService = subjectService;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<SubjectReq.SubjectInternalOutList> getByUserIdentifier(@RequestBody String userIdentifier) {
        return new ResponseEntity<>(subjectService.getSubjectsByUserIdentifier(userIdentifier), HttpStatus.OK);
    }
}
