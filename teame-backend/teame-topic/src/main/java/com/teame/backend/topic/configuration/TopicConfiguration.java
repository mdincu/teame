package com.teame.backend.topic.configuration;

import com.teame.backend.topic.converter.Category2CategoryInternalOutConverter;
import com.teame.backend.topic.converter.Subject2SubjectInternalOutConverter;
import com.teame.backend.topic.converter.Topic2TopicInternalOutConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import static com.teame.backend.topic.configuration.Constants.SCAN_PACKAGE;

@Configuration
@ComponentScan(SCAN_PACKAGE)
@EnableJpaRepositories(SCAN_PACKAGE)
public class TopicConfiguration {

    @Bean
    public ConversionService conversionService() {
        DefaultConversionService conversionService = new DefaultConversionService();
        conversionService.addConverter(new Topic2TopicInternalOutConverter());
        conversionService.addConverter(new Subject2SubjectInternalOutConverter());
        conversionService.addConverter(new Category2CategoryInternalOutConverter());
        return conversionService;
    }

}
