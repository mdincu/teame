package com.teame.backend.topic.controller;

import com.teame.backend.topic.service.TopicService;
import com.teame.backend.topic.api.model.request.TopicReq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/topic")
public class TopicController {

    private final TopicService topicService;

    @Autowired
    public TopicController(TopicService topicService) {
        this.topicService = topicService;
    }

    @RequestMapping(path = "/user/{user}", method = RequestMethod.GET)
    public ResponseEntity<TopicReq.TopicInternalOutList> getByUserIdentifier(@PathVariable("user") String userIdentifier) {
        return new ResponseEntity<>(topicService.getTopicsByUserIdentifier(userIdentifier), HttpStatus.OK);
    }

}
