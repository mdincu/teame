package com.teame.backend.topic.api.model.request;

import java.util.List;

public class CategoryReq {

    private String identifier;
    private String label;

    public CategoryReq() {
    }

    public CategoryReq(String identifier, String label) {
        this.identifier = identifier;
        this.label = label;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public static class CategoryInternalOutList {

        private List<CategoryReq> categoryReqs;

        public CategoryInternalOutList() {
        }

        public CategoryInternalOutList(List<CategoryReq> categoryReqs) {
            this.categoryReqs = categoryReqs;
        }

        public List<CategoryReq> getCategoryReqs() {
            return categoryReqs;
        }

        public void setCategoryReqs(List<CategoryReq> categoryReqs) {
            this.categoryReqs = categoryReqs;
        }
    }
}
