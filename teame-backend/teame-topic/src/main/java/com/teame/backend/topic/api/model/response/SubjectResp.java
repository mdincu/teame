package com.teame.backend.topic.api.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SubjectResp {

    @JsonProperty
    private String identifier;
    @JsonProperty
    private String label;
    @JsonProperty
    private String url;

    public SubjectResp() {
    }

    public SubjectResp(String identifier, String label, String url) {
        this.identifier = identifier;
        this.label = label;
        this.url = url;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
