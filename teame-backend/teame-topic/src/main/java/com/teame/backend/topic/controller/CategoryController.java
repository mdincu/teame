package com.teame.backend.topic.controller;

import com.teame.backend.topic.service.CategoryService;
import com.teame.backend.topic.api.model.request.CategoryReq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RequestMapping(value="/category", method = RequestMethod.POST)
public class CategoryController {

        private final CategoryService subjectService;

        @Autowired
        public CategoryController(CategoryService subjectService) {
            this.subjectService = subjectService;
        }

        @RequestMapping(method = RequestMethod.POST)
        public ResponseEntity<CategoryReq.CategoryInternalOutList> getByUserIdentifier(@RequestBody String userIdentifier) {
            return new ResponseEntity<>(subjectService.getCategoriesByUserIdentifier(userIdentifier), HttpStatus.OK);
        }
    }
