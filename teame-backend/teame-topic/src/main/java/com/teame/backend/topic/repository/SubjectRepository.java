package com.teame.backend.topic.repository;

import com.teame.backend.topic.entity.Subject;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SubjectRepository extends CrudRepository<Subject, String> {
    List<Subject> findByUserIdentifiers(String userIdentifier);
}
