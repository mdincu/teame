package com.teame.backend.topic.service;

import com.teame.backend.topic.entity.Subject;
import com.teame.backend.topic.repository.SubjectRepository;
import com.teame.backend.topic.api.model.request.SubjectReq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SubjectService {

    private final SubjectRepository subjectRepository;
    private final ConversionService conversionService;

    @Autowired
    public SubjectService(SubjectRepository subjectRepository, ConversionService conversionService) {
        this.subjectRepository = subjectRepository;
        this.conversionService = conversionService;
    }

    public SubjectReq.SubjectInternalOutList getSubjectsByUserIdentifier(String userIdentifier) {
        List<Subject> topics = subjectRepository.findByUserIdentifiers(userIdentifier);
        List<SubjectReq> topicInternalOuts = new ArrayList<>();
        for (Subject topic : topics) {
            topicInternalOuts.add(conversionService.convert(topic, SubjectReq.class));
        }
        return new SubjectReq.SubjectInternalOutList(topicInternalOuts);
    }

}
