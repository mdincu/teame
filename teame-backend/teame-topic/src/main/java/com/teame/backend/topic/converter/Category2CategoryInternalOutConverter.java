package com.teame.backend.topic.converter;

import com.teame.backend.topic.entity.Category;
import com.teame.backend.topic.api.model.request.CategoryReq;
import org.springframework.core.convert.converter.Converter;

public class Category2CategoryInternalOutConverter implements Converter<Category, CategoryReq> {
    @Override
    public CategoryReq convert(Category category) {

        return new CategoryReq(category.getIdentifier(), category.getLabel());

    }
}
