package com.teame.backend.topic.repository;

import com.teame.backend.topic.entity.Topic;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TopicRepository extends CrudRepository<Topic, String> {
    List<Topic> findByUserIdentifiers(String userIdentifier);
}
