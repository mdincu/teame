package com.teame.backend.topic.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "SUBJECT")
public class Subject {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String identifier;

    private String label;

    private String url;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "TOPIC_ID")
    private Topic topic;

    @ElementCollection
    private List<String> userIdentifiers;

    public Subject() {
    }

    public Subject(String label, String url, Topic topic, List<String> userIdentifiers) {
        this.label = label;
        this.url = url;
        this.topic = topic;
        this.userIdentifiers = userIdentifiers;
    }

    public String getIdentifier() {
        return identifier;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public List<String> getUserIdentifiers() {
        return userIdentifiers;
    }

    public void setUserIdentifiers(List<String> userIdentifiers) {
        this.userIdentifiers = userIdentifiers;
    }
}
