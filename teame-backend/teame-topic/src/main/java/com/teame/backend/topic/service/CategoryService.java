package com.teame.backend.topic.service;

import com.teame.backend.topic.entity.Category;
import com.teame.backend.topic.repository.CategoryRepository;
import com.teame.backend.topic.api.model.request.CategoryReq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryService {

    private final CategoryRepository subjectRepository;
    private final ConversionService conversionService;

    @Autowired
    public CategoryService(CategoryRepository subjectRepository, ConversionService conversionService) {
        this.subjectRepository = subjectRepository;
        this.conversionService = conversionService;
    }

    public CategoryReq.CategoryInternalOutList getCategoriesByUserIdentifier(String userIdentifier) {
        List<Category> topics = subjectRepository.findByUserIdentifiers(userIdentifier);
        List<CategoryReq> topicInternalOuts = new ArrayList<>();
        for (Category topic : topics) {
            topicInternalOuts.add(conversionService.convert(topic, CategoryReq.class));
        }
        return new CategoryReq.CategoryInternalOutList(topicInternalOuts);
    }
}
