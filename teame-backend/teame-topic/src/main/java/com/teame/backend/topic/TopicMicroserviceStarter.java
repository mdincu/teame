package com.teame.backend.topic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TopicMicroserviceStarter {
    public static void main(String[] args){
        SpringApplication.run(TopicMicroserviceStarter.class, args);
    }
}
