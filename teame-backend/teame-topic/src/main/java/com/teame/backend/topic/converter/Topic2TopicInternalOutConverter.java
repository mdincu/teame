package com.teame.backend.topic.converter;

import com.teame.backend.topic.entity.Topic;
import com.teame.backend.topic.api.model.request.TopicReq;
import org.springframework.core.convert.converter.Converter;

public class Topic2TopicInternalOutConverter implements Converter<Topic, TopicReq> {
    @Override
    public TopicReq convert(Topic topic) {

        return new TopicReq(topic.getIdentifier(),topic.getLabel());

    }
}
